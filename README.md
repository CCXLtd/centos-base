# Base Centos Docker Image for Code Red

This will build a base Docker Centos image using  [codered/centos-minimal](https://hub.docker.com/r/codered/centos-minimal/)

## Build
It is recommended to use a proxy to reduce bandwidth usage. See the Dockerfile.


## To Build

```
# normal build
docker build --tag codered/centos-base .
```

```
# force a full build
docker build --no-cache=true --force-rm=true --tag codered/centos-base .
```

```
# send it to docker hub
> docker push codered/centos-base
```

```
# To Run
>  docker run -t -i codered/centos-base
```

[1]:  http://www.centos.com//
